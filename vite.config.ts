import { defineConfig } from 'vite'
import vue from '@vitejs/plugin-vue'
import * as path from 'path';
import postCssPxToRem from "postcss-pxtorem";
import AutoImport from 'unplugin-auto-import/vite'
import Components from 'unplugin-vue-components/vite'
import { ElementPlusResolver } from 'unplugin-vue-components/resolvers'
import viteCompression from 'vite-plugin-compression';
import viteImagemin from 'vite-plugin-imagemin'
import { visualizer } from 'rollup-plugin-visualizer';
import { autoComplete, Plugin as importToCDN } from 'vite-plugin-cdn-import';
// https://vitejs.dev/config/
export default defineConfig({
  resolve: {
    //设置别名
    alias: {
        '@': path.resolve(__dirname, 'src')
    }
},
  plugins: [vue(), 
    AutoImport({
      resolvers: [ElementPlusResolver()],
    }),
    Components({
      resolvers: [ElementPlusResolver()],
    }),
    // 开启GZIP压缩
    viteCompression({
      verbose: true, //是否在控制台输出压缩结果
      disable: false, //是否禁用,相当于开关在这里
      threshold: 10240, //体积大于 threshold 才会被压缩,单位 b，1b=8B, 1B=1024KB  那我们这里相当于 9kb多吧，就会压缩
      algorithm: 'gzip', //压缩算法,可选 [ 'gzip' , 'brotliCompress' ,'deflate' , 'deflateRaw']
      ext: '.gz', //文件后缀
    }),
    // 开启性能分析的插件
    visualizer({
      emitFile: false,
      file: "stats.html", //分析图生成的文件名
      open:true //如果存在本地服务端口，将在打包后自动展示
    }), 
    //使用CDN缓存
    importToCDN({
      prodUrl: 'https://unpkg.com/{name}@{version}/{path}',
      modules: [
        autoComplete('vue'),
        autoComplete('axios'),
        {
          name: 'element-plus',
          var: 'ElementPlus', //根据main.js中定义的来
          version: '2.3.1',
          path: 'dist/index.full.js',
          css: 'dist/index.css'
        },
        {
          name: '@element-plus/icons-vue',
          var: 'ElementPlusIconsVue', //根据main.js中定义的来
          version: '2.0.9',
          path: 'dist/index.iife.min.js'
        },
        // {
        //   name: 'echarts',
        //   var: 'echarts', //根据main.js中定义的来
        //   version: '5.4.1',
        //   path: 'https://cdn.jsdelivr.net/npm/echarts@5.4.1/dist/echarts.min.js',
        // }
      ],
    }), viteImagemin({
      gifsicle: {
        optimizationLevel: 7,
        interlaced: false
      },
      optipng: {
        optimizationLevel: 7
      },
      mozjpeg: {
        quality: 20
      },
      pngquant: {
        quality: [0.8, 0.9],
        speed: 4
      },
      svgo: {
        plugins: [
          {
            name: 'removeViewBox'
          },
          {
            name: 'removeEmptyAttrs',
            active: false
          }
        ]
      }
    })
                 //  原文链接：https://blog.csdn.net/qq_43806604/article/details/124732352
  ],
  server: {
    port: 8080, //启动端口
    hmr: {
        host: '127.0.0.1',
        port: 8080
    },
    // 设置 https 代理
    proxy: {
        '/api': {
            target: 'your https address',
            changeOrigin: true,
            rewrite: (path: string) => path.replace(/^\/api/, '')
        }
    }
},
css: {
  postcss: {
      plugins: [
          postCssPxToRem({
              rootValue: 192, // （设计稿/10）1rem的大小
              propList: ['*'], // 需要转换的属性，这里选择全部都进行转换
          })
      ]
  }
},
build: {
  rollupOptions: {
    output: {
      // ... 其他配置

      // chunk 文件名
      chunkFileNames: "assets/js/[name].[hash].js",
      assetFileNames: (chunkInfo) => {
        // 用后缀名称进行区别处理
        // 处理其他资源文件名 e.g. css png 等
        let subDir = "images";
        
        if (path.extname(chunkInfo.name) === ".css") {
          subDir = "css";
        }
        
        return `assets/${subDir}/[name].[hash].[ext]`;
      },
      // 入口文件名
      entryFileNames: "assets/js/[name].[hash].js",
      sourcemap: true,
    },
  },
  assetsDir: "assets",
  // ... 其他配置
}
})
