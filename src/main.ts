// import tools from './plugins/echarts'

import { createApp } from 'vue'
// import './style.css'
import App from './App.vue'
import router from '@/router';
// 不支持vite形式,里面不是export default抛出的.希望后续支持吧.下面是补丁包
// import  dataV from '@jiaminghi/data-view'
import DataVVue3 from '@kjgl77/datav-vue3'
import 'amfe-flexible'
// 引入Element Plus组件库
import ElementPlus from 'element-plus'
import 'element-plus/dist/index.css'
// 引入全局css
import './assets/scss/style.scss';
// 导入公共方法
import {codeToLonLat} from "@/utils/utils.js"
// 创建axios echarts全局挂载
import * as echarts from 'echarts';
import axios from 'axios'
// 引入 vue-3d-loader 
// import vue3dLoader from "vue-3d-loader";


// import 'echarts/map/js/china.js'
// 创建vue实例
const app = createApp(App);

app.config.globalProperties.$echarts = echarts;
app.config.globalProperties.$axios = axios;

app.use(router);
app.use(DataVVue3)
app.use(ElementPlus)
// app.use(vue3dLoader)
// vue3中挂载到全局的方式
// app.config.globalProperties.$tools = tools;
app.config.globalProperties.codeToLonLat = codeToLonLat;
// app.use(dataV)
 // 挂载实例
 app.mount('#app');
