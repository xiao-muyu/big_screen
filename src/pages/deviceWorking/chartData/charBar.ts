//场地使用
export const charLeftBottom={
    xAxis: {
      type: 'category',
      data: null,
    },
    tooltip:{
      show:true,
      trigger:'axis'
    },
    yAxis: {
      type: 'value',
      splitLine: {
        //网格线
        lineStyle: {
          type: 'solid', //设置网格线类型 dotted：虚线 solid:实线
          color:'#eee'
        },
        show: true, //隐藏或显示
      },
      show: true
    },
    series: [
      {
        data: null,
        type: 'bar',
        color: "#7C1AFF",
        itemStyle:{
          normal: {
            color: {
                type: 'linear',
                x: 0,
                y: 0,
                x2: 0,
                y2: 1,
                colorStops: [
                    {
                        offset: 0,
                        color: '#C000FF', //  0%  处的颜色
                    },
                    {
                        offset: 1,
                        color: '#203AFF', //  100%  处的颜色
                    },
                ],
                global: false, //  缺省为  false
            },
            barBorderRadius: [30, 30, 0, 0],
            shadowColor: 'rgba(0,160,221,1)',
            shadowBlur: 4,
        },
        }
      }
    ]
};
//实时用电
export const charLeftCenter={
    xAxis: {
      type: 'category',
      data: ['2', '4', '6', '8', '10', '12', '14', '16', '18', '20', '22', '24'],
    },
    tooltip:{
      show:true,
      trigger:'axis'
    },
    yAxis: {
      type: 'value',
      splitLine: {
        //网格线
        lineStyle: {
          type: 'sloid', //设置网格线类型 dotted：虚线 solid:实线
        },
        show: false, //隐藏或显示
      },
    },
    series: [
      {
        data: null,
        type: 'line',
        smooth: true,
        // color: "red",
        showSymbol: false,
        areaStyle: {},
        itemStyle: {
            normal: {
                show: false,
                color: "#761057", //改变折线点的颜色
                lineStyle: {
                    color: "#f00" //改变折线颜色
                },
                label: {
                    show: false, //开启显示
                    position: 'top', //在上方显示
                    textStyle: { //数值样式
                        color: '#999999',
                        fontSize: 10
                    }
                },

            },
      },
    },
    ]
};
//设备统计
export const charLeftTop={
    tooltip: {
      trigger: 'item',
    },
    legend: {
      left: 'right',
      type: 'scroll',
      orient: 'vertical',
      textStyle:{
        color:'#fff'
      }
    },

    series: [
      {
        roseType: 'area',
        type: 'pie',
        itemStyle: {
          borderRadius: 8
        },
        label: {
          show: false
        },
        data: null,
        Symbol: false
      }
    ]
  };


// 设备检测
  export const charRightTop={
    
  };