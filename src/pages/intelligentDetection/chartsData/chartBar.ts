import * as echarts from "echarts";
// 水电用量测量数据
export const chartBarData ={
  
  tooltip: {
    trigger: 'axis',
    axisPointer: {
      type: 'shadow'
    }
  },
  legend: {
    icon: 'circle',
    textStyle: {
      color: "#fff",
      fontSize:'10px'
  },
  itemWidth: 20,
  itemHeight: 20,
      // data: ['安防设备用电', '教学用水', '生态用水','生活用水'],
  },
  grid: {
    left: '3%',
    right: '4%',
    bottom: '3%',
    containLabel: true
  },
 
 
  // xAxis:{
   
  //   type: 'value',
  //   position: 'bottom',
  //   data: ['一月', '二月', '三月', '四月', '五月', '六月'],
  //   axisLine: {       // 坐标轴 轴线
  //     show: true, }

  // } ,
  // yAxis: {
  //   min:0,//设置最小值
  //   max:100000,//设置最大值
  //   type:'category',
   
   
  // },
//   series: [
//     {
//         name: "教学用水",
//         type: "bar",
//         barWidth:5,
//         itemStyle: {
//           normal: {
//             color: new echarts.graphic.LinearGradient(0, 0, 0, 1, [{
//                offset: 0,
//                 color: '#D200A3'
//             }, {
//               offset: 1,
//               color: '#FF0044'
//           }]),
//           barBorderRadius: 12
//         }
//       },
//         data: [
//             1150,
//             1590,
//             1680,
//             1590
//         ]
//     },
//     {
//         name: "安防设备用电",
//         type: "bar",
//         barWidth: 10,
//         itemStyle: {
//           normal: {
//             color: new echarts.graphic.LinearGradient(0, 0, 0, 1, [{
//                offset: 0,
//                 color: '#203AFF'
//             }, {
//               offset: 1,
//               color: '#C000FF'
//           }]),
//           barBorderRadius: 12
//         }
//       },
//         data: [
//             1590,
//             1680,
//             1680,
//             1590
//         ]
//     },
//     {
//         name: "生态用水",
//         type: "bar",
//         barWidth: 10,
//         itemStyle: {
//           normal: {
//             color: new echarts.graphic.LinearGradient(0, 0, 0, 1, [{
//                offset: 0,
//                 color: '#203AFF'
//             }, {
//               offset: 1,
//               color: '#C000FF'
//           }]),
//           barBorderRadius: 12
//         }
//       },
//         data: [
//             1680,
//             1590,
//             1150,
//             1590
//         ]
//     },
//     {
//         name: "生活用水",
//         type: "bar",
//         barWidth: 10,
//         itemStyle: {
//           normal: {
//             color: new echarts.graphic.LinearGradient(0, 0, 0, 1, [{
//                offset: 0,
//                 color: '#203AFF'
//             }, {
//               offset: 1,
//               color: '#C000FF'
//           }]),
//           barBorderRadius: 12
//         }
//       },
//         data: [
//             1590,
//             1590,
//             1680,
//             1150
//         ]
//     }
// ] ,
};
//校园人流行动
export const chartRightBottom={
legend: {
  icon: 'circle',
  textStyle:{
    color:"#fff"},
    itemWidth: 10,
    itemHeight: 10,
},
tooltip: {},
dataset: {
  dimensions: null,
  source:null
},
xAxis: { type: 'category' },
yAxis: {},
// Declare several bar series, each will be mapped
// to a column of dataset.source by default.
series: [
  { 
    type: 'bar',
    barWidth: 10,
    itemStyle: {
      normal: {
        color: new echarts.graphic.LinearGradient(0, 0, 0, 1, [{
           offset: 0,
            color: '#203AFF'
        }, {
          offset: 1,
          color: '#C000FF'
      }]),
      barBorderRadius: 12
    }
  }
 }, { 
  type: 'bar',
  barWidth: 10,
    itemStyle: {
      normal: {
        color: new echarts.graphic.LinearGradient(0, 0, 0, 1, [{
           offset: 0,
            color: '#40E496'
        }, {
          offset: 1,
          color: '#029A0E'
      }]),
      barBorderRadius: 12
    }
  }
 }]
};



//室内环境检测
export const charLeftTop={
tooltip: {
  trigger: "axis",
  axisPointer: {
    // 坐标轴指示器，坐标轴触发有效
    type: "shadow", // 默认为直线，可选为：'line' | 'shadow'
  },
},
grid: null,
legend: null,
xAxis: null,
yAxis: {
  type: "category",
  data: null,
  max: "1200",
  splitLine: {
    show: false,
    lineStyle: {
      color: "rgba(255,255,255,0.3)",
    },
  },
  axisLabel: {
    textStyle: {
      color: "#fff",
    }
  },
},
series: [
  {
    name: "2019",
    type: "bar",
    barWidth: "15%",
    itemStyle: {
      normal: {
        color: new echarts.graphic.LinearGradient(0, 0, 0, 1, [
          {
            offset: 0,
            color: "#fccb05",
          },
          {
            offset: 1,
            color: "#f5804d",
          },
        ]),
        barBorderRadius: 12,
      },
    },
    data: [400, 400, 300, 300, 300, 400, 400, 400, 300],
  },
  {
    name: "2020",
    type: "bar",
    barWidth: "15%",
    itemStyle: {
      normal: {
        color: new echarts.graphic.LinearGradient(0, 0, 0, 1, [
          {
            offset: 0,
            color: "#8bd46e",
          },
          {
            offset: 1,
            color: "#09bcb7",
          },
        ]),
        barBorderRadius: 11,
      },
    },
    data: [400, 500, 500, 500, 500, 400, 400, 500, 500],
  },
  {
    name: "2021",
    type: "bar",
    barWidth: "15%",
    itemStyle: {
      normal: {
        color: new echarts.graphic.LinearGradient(0, 0, 0, 1, [
          {
            offset: 0,
            color: "#248ff7",
          },
          {
            offset: 1,
            color: "#6851f1",
          },
        ]),
        barBorderRadius: 11,
      },
    },
    data: [400, 600, 700, 700, 1000, 400, 400, 600, 700],
  },
],
}
//学生分布情况
export const charLeftBottom={
    tooltip: {
    trigger: 'axis'
  },
  legend: {
    textStyle: {
      color: 'white',
      fontSize: 8
    },
    icon: 'circle',
    data: null
  },
  grid: {
    left: '3%',
    right: '4%',
    bottom: '3%',
    containLabel: true
  },
  // toolbox: {
  //   feature: {
  //     saveAsImage: {}
  //   }
  // },
  xAxis: {
    type: 'category',
    boundaryGap: false,
    data: null,
    // axisLabel: {
    //   rotate: 60
    // }
  },
  yAxis: {
    type: 'value'
  },
  series: null
  };