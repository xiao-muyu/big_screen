import * as echarts from "echarts";
// 毕业生工作状态
var color = ['#00FF7D', '#00A3FE', '#9665EB', "#E2CA72", "#F76160"]
var data = [
  { value: 1000, name: '已就业' },
  { value: 580, name: '未就业' },
  { value: 188, name: '返回未就业' },
  { value: 188, name: '以往未就业' },
  { value: 188, name: '返校重听' },
];

export const charLeftBottom = {
  color: color,
  backgroundColor: '#0A173B',

  tooltip: {
    trigger: 'item'
  },
  series: [{
    type: 'pie',
    roseType: 'radius',
    radius: ['30%', '60%'],
    data: data,
    itemStyle: {
      normal: {
        label: {
          show: true,
          formatter: '{b}{d}'//显示格式
        },
        labelLine: { show: true }
      }
    },
    labelLine: {
      lineStyle: {
        color: '#fff'
      }
    },
    emphasis: {
      itemStyle: {
        shadowBlur: 5,
        shadowOffsetX: 0,
        shadowColor: 'rgba(0,0,0,0.5)'
      }
    }
  }]
}


// [
//   { value: 1000, name: '已就业92%' },
//   { value: 580, name: '未就业 8%' },
//   { value: 188, name: '返回未就业 2%' },
//   { value: 188, name: '以往未就业 2%' },
//   { value: 188, name: '返校重听 2%' },
// ],
//{
//     grid: {
//       left: "0",
//       right: "0",
//       bottom: "0",
//       top: "0",
//       containLabel: true,
//     },
//     legend: {
//       top: 'bottom',
//       textStyle: {
//         color: "#fff",
//       },
//     },
//     color: ["#00ffff",
//       "#00cfff",
//       "#006ced",
//       "#ffe000",
//       "#ffa800",
//       "#ff5b00",
//       "#ff3000",],
//     series: [
//       {
//         name: 'Nightingale Chart',
//         type: 'pie',
//         radius: [40, 100],
//         center: ['48%', '45%'],
//         roseType: 'area',
//         shadowColor: 'rgba(0,0,0,0.8)',
//         shadowBlur: 20,
//         shadowOffsetY: 20,
//         data: [
//           { value: 1000, name: '已就业92%' },
//           { value: 380, name: '未就业 8%' },
//           { value: 320, name: '返回未就业 2%' },
//           { value: 188, name: '以往未就业 2%' },
//           { value: 28, name: '返校重听 2%' },
//         ]
//       }
//     ]
// };

// 近3年来毕业人员去向
export const charLeftTop = {
  tooltip: {
    trigger: "axis",
    axisPointer: {
      // 坐标轴指示器，坐标轴触发有效
      type: "shadow", // 默认为直线，可选为：'line' | 'shadow'
    },
  },
  grid: {
    left: "0",
    right: "0",
    bottom: "14%",
    top: "4%",
    containLabel: true,
  },

  xAxis: {
    type: "value",
    splitLine: {
      show: false,
    },
    axisTick: {
      show: true,
    },
    splitArea: {
      show: false,
    },
    axisLabel: {
      interval: 0,
      show: true, // 不显示坐标轴上的文字
    },
  },

  yAxis: {
    scale: true,
    splitNumber: 2,
    boundaryGap: [0.2, 0.2],
    type: "category",
    // type: "value",
    data: ["湖北", "江苏", "浙江", "广西", "广东", "重庆", "天津", "上海"],
    // max: 10,
    splitLine: {
      show: false,
      lineStyle: {
        color: "rgba(255,255,255,0.3)",
      },
    },
    axisLabel: {
      // x轴的文字改为竖版显示
      // formatter: function (value) {
      //   var str = value.split("");
      //   return str.join("\n");
      // },
      textStyle: {
        color: "#fff",
      }
    },
  },
  series: [
    {
      name: "2019",
      type: "bar",
      barWidth: "15%",
      itemStyle: {
        normal: {
          color: new echarts.graphic.LinearGradient(0, 0, 1, 0, [
            {
              offset: 0,
              color: "#A520FF",
            },
            {
              offset: 1,
              color: "#FF007A",
            },
          ]),
          barBorderRadius: 12,
        },
      },
      data: [4500, 3250, 2750, 3500, 6000, 4000, 3200, 3750],

    },
    {
      name: "2020",
      type: "bar",
      barWidth: "15%",
      itemStyle: {
        normal: {
          color: new echarts.graphic.LinearGradient(0, 0, 1, 0, [
            {
              offset: 0,
              color: "#00A2FF",
            },
            {
              offset: 1,
              color: "#00CCD2",
            },
          ]),
          barBorderRadius: 11,
        },
      },
      data: [3500, 2500, 2000, 2750, 4750, 3200, 2450, 2800],
    },
    {
      name: "2021",
      type: "bar",
      barWidth: "15%",
      itemStyle: {
        normal: {
          color: new echarts.graphic.LinearGradient(0, 0, 1, 0, [
            {
              offset: 0,
              color: "#6A6660",
            },
            {
              offset: 1,
              color: "#FFC400 ",
            },
          ]),
          barBorderRadius: 11,
        },
      },
      data: [4100, 3100, 2500, 3200, 5750, 3800, 3000, 3500],
    },
  ],
}
// 毕业生薪资分布

//毕业生流向城市
export const charRightTop = {
  dataZoom: [{
    type: 'slider',
    //backgroundColor: 'rgba(245,245,245)',
    backgroundColor: 'transparent',
    brushSelect: false,
    width: 3,
    show: false, //flase直接隐藏图形
    yAxisIndex: [0],
    //left: 'center', //滚动条靠左侧的百分比
    //bottom: 13,
    startValue: 0, //滚动条的起始位置
    endValue: 4, //滚动条的截止位置（按比例分割你的柱状图x轴长度）
    handleStyle: {
      color: '#006ced',
      borderColor: '#E8E8E8',
    },
    fillerColor: '#E8E8E8',
    borderColor: 'transparent',
    showDetail: false,

    dataBackground: {
      areaStyle: {
        opacity: 0
      },
      lineStyle: {
        color: 'transparent'
      },
    },

  }],
  legend: {
    show: false,
  },
  grid: {
    left: "10%",
    right: "20%",
    bottom: "5%",
    top: "8%",
    containLabel: true,
  },
  xAxis: [
    {
      splitLine: {
        show: false,
      },
      type: "value",
      show: false,
    },
  ],
  yAxis: [
    {
      splitLine: {
        show: false,
      },
      axisLine: {//y轴
        show: false,
      },
      type: "category",
      axisTick: {
        show: false,
      },
      inverse: true, // 倒序
      boundaryGap: false, // 留白政策
      data: null,
      axisLabel: {
        color: "rgba(0, 0, 0, 0.65)",
        align: 'left',
        padding: [-10, 0, 0, -110],
        verticalAlign: "top",
        formatter: (value, index) => {
          let i = nameList.indexOf(value);
          // console.log(i)
          if (i == 0) {
            return [`{lg1|${i + 1}} ` + "{title|" + value + "} "].join("\n");
          } else if (i == 1) {
            return [`{lg2|${i + 1}} ` + "{title|" + value + "} "].join("\n");
          } else if (i == 2) {
            return [`{lg3|${i + 1}} ` + "{title|" + value + "} "].join("\n");
          } else {
            return [`{lg|${i + 1}} ` + "{title|" + value + "} "].join("\n");
          }
        },
        rich: {
          lg1: {
            backgroundColor: "rgba(240, 106, 57, 0.1)",
            color: 'rgba(240, 106, 57, 1)',
            borderRadius: [5, 5, 0, 0],
            padding: 5,
            align: "center",
            width: 32,
            height: 32,
            lineHeight: 32,
            fontSize: 19,
            fontFamily: 'Source Han Sans CN-Regular',
          },
          lg2: {
            backgroundColor: "rgba(255, 176, 38, 0.1)",
            color: 'rgba(255, 176, 38, 1)',
            borderRadius: 5,
            padding: 5,
            align: "center",
            width: 32,
            height: 32,
            lineHeight: 32,
            fontSize: 19,
            fontFamily: 'Source Han Sans CN-Regular',
          },
          lg3: {
            backgroundColor: "rgba(51, 207, 201, 0.1)",
            color: 'rgba(51, 207, 201, 1)',
            borderRadius: 5,
            padding: 5,
            align: "center",
            width: 32,
            height: 32,
            lineHeight: 32,
            fontSize: 19,
            fontFamily: 'Source Han Sans CN-Regular',
          },
          lg: {
            backgroundColor: "rgba(57, 126, 240, 0.1)",
            color: 'rgba(57, 126, 240, 1)',
            borderRadius: 5,
            padding: 5,
            align: "center",
            width: 32,
            height: 32,
            lineHeight: 32,
            fontSize: 19,
            fontFamily: 'Source Han Sans CN-Regular',
          },
          title: {
            color: 'rgba(255,255,255,0.65)',
            align: "right",
            fontSize: 18,
            fontFamily: 'Source Han Sans CN-Regular',
            padding: [0, 0, 0, 21],
          },
        },
      },
    },
  ],
  series: [
    {
      name: "人数",
      type: "bar",
      barWidth: 13, // 柱子宽度
      showBackground: true,
      left: -200,
      backgroundStyle: {
        color: 'rgba(57, 126, 240, 0)',
        borderColor: 'rgba(57, 126, 240, 0.04)',
        borderWidth: 20
      },
      label: {
        show: true,
        formatter: '{c}人',
        color: "rgba(255, 255, 255, 0.85)",
        fontFamily: "HarmonyOS Sans-Medium",
        fontSize: 14,
        position: 'right'
      },
      itemStyle: {

        barBorderRadius: [0, 3, 3, 0], // 圆角（左上、右上、右下、左下）
        color: {
          x: 0,
          y: 1,
          x2: 1,
          y2: 0,
          colorStops: [{
            offset: 0,
            color: 'rgba(57, 126, 240, 1)' // 0% 处的颜色
          }, {
            offset: 1,
            color: 'rgba(51, 207, 201, 1)' // 100% 处的颜色
          }],
        },
      },
      data: null,
    },
  ],
};

// 毕业生薪资分布
export const charRightBottom = {

  legend: {
    data: ['10K-', '10K~15k', "15K~20k", "20K-30K", "30K+"],
    lineStyle: { type: 'circle' },
    textStyle: {
      color: 'white'
    }
  },
  color: ['#01C3D5 ', '#E46666 ', '#4CB042', '#A05FFD ', '#FFAE00 '],
  series: [
    {
      name: 'Funnel',
      type: 'funnel',
      left: '-6%',
      top: 60,
      bottom: 60,
      width: '80%',
      min: 0,
      max: 30,
      minSize: '0%',
      maxSize: '100%',
      sort: 'descending',
      gap: 2,
      label: {
        show: true,
        position: 'inside'
      },
      labelLine: {
        length: 10,
        lineStyle: {
          width: 1,
          type: 'solid'
        }
      },
      itemStyle: {
        borderColor: '#fff',
        borderWidth: 1
      },
      emphasis: {
        label: {
          fontSize: 20
        }
      },
      data: null
    },

    {
      name: '数值',
      type: 'funnel',
      left: '57%',
      y: 100,
      y2: 100,
      width: '0%',
      min: 100,
      max: 0,
      minSize: '0%',
      maxSize: '100%',
      gap: 20,
      sort: 'ascending',
      data: null,
      // data:numberArr.value.sort(function (a:any, b:any) {
      //     return a.value - b.value;
      // }),
      label: {
        normal: {
          formatter: function (params) {
            return params.name + "人";
          },
          backgroundColor: 'rgba(255,255,255,0)',
          // backgroundColor: 'rgba(255,255,255,0.4)',
          color: '#fff',
          padding: [80, 80, 81, 80],
          shadowBlur: 0,
          shadowOffsetX: 6,
          shadowOffsetY: 6,
          shadowColor: '#fff',
          textStyle: {
            color: '#fff',
            fontSize: 16,
          },
        },
      },
    }, {
      name: '标签',
      type: 'funnel',
      left: '57%',
      y: 100,
      y2: 100,
      width: '0%',
      min: 100,
      max: 0,
      minSize: '0%',
      maxSize: '100%',
      gap: 10,
      sort: 'ascending',
      // data: tagArr.value.sort(function (a:any, b:any) {
      //     return a.value - b.value;
      // }),
      data: null,
      label: {
        normal: {
          formatter: function (params) {
            return params.name;
          },
          backgroundColor: 'rgb(86,111,206)',
          color: '#fff',
          borderRadius: 4,
          padding: [5, 10, 5, 3],
          shadowBlur: 0,
          shadowOffsetX: 2,
          shadowOffsetY: 2,
          shadowColor: '#fff',
          textStyle: {
            color: '#fff',
            fontSize: 12,
          },
        },
      },
      labelLine: {
        normal: {
          show: true,
          length: 10,
          lineStyle: {
            type: 'solid',
            color: '#fff',
          },
        },
      },

      itemStyle: {
        normal: {
          opacity: 1,
          borderWidth: 1,
          shadowBlur: 100,
          shadowOffsetX: 0,
          shadowOffsetY: 10,
          shadowColor: 'rgba(0, 0, 0, 0.5)',
        },
      },
    },
  ]
}
// {
//       dataZoom: [{
//          type: 'slider',
//          //ckgroundColor: 'rgba(245,245,245)',
//          backgroundColor: 'transparent',
//          brushSelect: false,
//          width: 3,
//          show: false, //flase直接隐藏图形
//          yAxisIndex: [0],
//          //left: 'center', //滚动条靠左侧的百分比
//          //bottom: 13,
//          startValue: 0, //滚动条的起始位置
//          endValue: 4, //滚动条的截止位置（按比例分割你的柱状图x轴长度）
//          handleStyle: {
//             color: '#006ced',
//             borderColor: '#E8E8E8',
//          },
//          fillerColor: '#E8E8E8',
//          borderColor: 'transparent',
//          showDetail: false,

//          dataBackground: {
//             areaStyle: {
//                opacity: 0
//             },
//             lineStyle: {
//                color: 'transparent'
//             },
//          },

//       }],
//       legend: {
//          show: false,
//       },
//       grid: {
//          left: "10%",
//          right: "20%",
//          bottom: "5%",
//          top: "8%",
//          containLabel: true,
//       },
//       xAxis: [
//          {
//             splitLine: {
//                show: false,
//             },
//             type: "value",
//             show: false,
//          },
//       ],
//       yAxis: [
//          {
//             splitLine: {
//                show: false,
//             },
//             axisLine: {//y轴
//                show: false,
//             },
//             type: "category",
//             axisTick: {
//                show: false,
//             },
//             inverse: true, // 倒序
//             boundaryGap: false, // 留白政策
//             data: nameList,
//             axisLabel: {
//                color: "rgba(0, 0, 0, 0.65)",
//                align: 'left',
//                padding: [-10, 0, 0, -110],
//                verticalAlign: "top",
//                formatter: (value, index) => {
//                   let i = nameList.indexOf(value);
//                   // console.log(i)
//                   if (i == 0) {
//                      return [`{lg1|${i + 1}} ` + "{title|" + value + "} "].join("\n");
//                   } else if (i == 1) {
//                      return [`{lg2|${i + 1}} ` + "{title|" + value + "} "].join("\n");
//                   } else if (i == 2) {
//                      return [`{lg3|${i + 1}} ` + "{title|" + value + "} "].join("\n");
//                   } else {
//                      return [`{lg|${i + 1}} ` + "{title|" + value + "} "].join("\n");
//                   }
//                },
//                rich: {
//                   lg1: {
//                      backgroundColor: "rgba(240, 106, 57, 0.1)",
//                      color: 'rgba(240, 106, 57, 1)',
//                      borderRadius: [5, 5, 0, 0],
//                      padding: 5,
//                      align: "center",
//                      width: 32,
//                      height: 32,
//                      lineHeight: 32,
//                      fontSize: 19,
//                      fontFamily: 'Source Han Sans CN-Regular',
//                   },
//                   lg2: {
//                      backgroundColor: "rgba(255, 176, 38, 0.1)",
//                      color: 'rgba(255, 176, 38, 1)',
//                      borderRadius: 5,
//                      padding: 5,
//                      align: "center",
//                      width: 32,
//                      height: 32,
//                      lineHeight: 32,
//                      fontSize: 19,
//                      fontFamily: 'Source Han Sans CN-Regular',
//                   },
//                   lg3: {
//                      backgroundColor: "rgba(51, 207, 201, 0.1)",
//                      color: 'rgba(51, 207, 201, 1)',
//                      borderRadius: 5,
//                      padding: 5,
//                      align: "center",
//                      width: 32,
//                      height: 32,
//                      lineHeight: 32,
//                      fontSize: 19,
//                      fontFamily: 'Source Han Sans CN-Regular',
//                   },
//                   lg: {
//                      backgroundColor: "rgba(57, 126, 240, 0.1)",
//                      color: 'rgba(57, 126, 240, 1)',
//                      borderRadius: 5,
//                      padding: 5,
//                      align: "center",
//                      width: 32,
//                      height: 32,
//                      lineHeight: 32,
//                      fontSize: 19,
//                      fontFamily: 'Source Han Sans CN-Regular',
//                   },
//                   title: {
//                      color: 'rgba(255,255,255,0.65)',
//                      align: "right",
//                      fontSize: 18,
//                      fontFamily: 'Source Han Sans CN-Regular',
//                      padding: [0, 0, 0, 21],
//                   },
//                },
//             },
//          },
//       ],
//       series: [
//          {
//             name: "人数",
//             type: "bar",
//             barWidth: 13, // 柱子宽度
//             showBackground: true,
//             left: -200,
//             backgroundStyle: {
//                color: 'rgba(57, 126, 240, 0)',
//                borderColor: 'rgba(57, 126, 240, 0.04)',
//                borderWidth: 20
//             },
//             label: {
//                show: true,
//                formatter: '{c}人',
//                color: "rgba(255, 255, 255, 0.85)",
//                fontFamily: "HarmonyOS Sans-Medium",
//                fontSize: 14,
//                position: 'right'
//             },
//             itemStyle: {

//                barBorderRadius: [0, 3, 3, 0], // 圆角（左上、右上、右下、左下）
//                color: {
//                   x: 0,
//                   y: 1,
//                   x2: 1,
//                   y2: 0,
//                   colorStops: [{
//                      offset: 0,
//                      color: 'rgba(57, 126, 240, 1)' // 0% 处的颜色
//                   }, {
//                      offset: 1,
//                      color: 'rgba(51, 207, 201, 1)' // 100% 处的颜色
//                   }],
//                },
//             },
//             data: blList,
//          },
//       ],
//    };
