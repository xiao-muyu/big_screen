import { createRouter, createWebHistory, RouteRecordRaw } from 'vue-router';
import jobDirection from '@/pages/jobDirection/index.vue'
const routes: Array<RouteRecordRaw> = [
  {
    path: '/',
    redirect: "/intellect",
  },
  {
    path: '/index',
    name: 'Index',
    redirect: 'intellect',
    meta: {
      title: '校园概览',
      keepAlive: true,
      requireAuth: true
    },
    component: () => import('@/pages/index/index.vue'),
    children: [{
      path: '/intelligentDetection',
      name: 'intelligentDetection',
      meta: {
        title: '智能检测',
        keepAlive: true,
        requireAuth: true
      },
      component: () => import('@/pages/intelligentDetection/index.vue'),
    },
    {
      path: '/jobDirection',
      name: 'jobDirection',
      meta: {
        title: '就业态势',
        keepAlive: true,
        requireAuth: true
      },
      //        component: () => import('@/pages/jobDirection/index.vue'),
      component: jobDirection,
    },
    {
      path: '/safeManage',
      name: 'safeManage',
      meta: {
        title: '安防管理',
        keepAlive: true,
        requireAuth: true
      },
      component: () => import('@/pages/safeManage/index.vue'),
    }, {
      path: '/deviceWorking',
      name: 'deviceWorking',
      meta: {
        title: '设备运维',
        keepAlive: true,
        requireAuth: true
      },
      component: () => import('@/pages/deviceWorking/index.vue'),
    }, {
      path: '/equipment',
      name: 'equipment',
      meta: {
        title: '设备运维',
        keepAlive: true,
        requireAuth: true
      },
      component: () => import('@/pages/equipment/index.vue'),
    }, {
      path: '/intellect',
      name: 'intellect',
      meta: {
        title: '智能检测',
        keepAlive: true,
        requireAuth: true
      },
      component: () => import('@/pages/intellect/index.vue'),
    }, {
      path: '/administration',
      name: 'administration',
      meta: {
        title: '就业态势',
        keepAlive: true,
        requireAuth: true
      },
      component: () => import('@/pages/administration/index.vue'),
    }, {
      path: '/situation',
      name: 'situation',
      meta: {
        title: '安防管理',
        keepAlive: true,
        requireAuth: true
      },
      component: () => import('@/pages/situation/index.vue'),
    }
    ]
  }
]

const router = createRouter({
  history: createWebHistory(),
  routes,
});
export default router;

