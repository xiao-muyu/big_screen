// echarts.js
import echarts from 'echarts'
const tools = {
  chinaMap:function(id,data){
    var myChart = echarts.init(document.getElementById(id));
    var option = {
      // 提示信息
      tooltip:{
		formatter(data){
          return "<div><p>"+data.name+"</p><p>现存确诊:"+data.value+"</p></div>"
        }
	  },
      // 资源信息
      series:[{
        name:"省",
        type:"map",
        map:"china",
        roam:false,// 是否允许自动缩放
        zoom:1.2,// 地图缩放的比例
        label:{ // 配置字体
          normal:{
            show:true,
            textStyle:{
              fontsize:8
            }
          }
        },
        itemStyle:{ //配置地图样式
          normal:{
            areaColor:'rgba(0,255,236,0)',
            borderColor:'rgb(0,0,0,0.2)'
          },
          emphasis: { // 选中的区域颜色及阴影效果等
            areaColor: 'rgba(255,180,0,0.8)',
            shadowOffsetX: 0,
            shadowOffsetY: 0,
            shadowBlur: 20,
            borderWidth: 0,
            shadowColor:'rgba(0,0,0,0.5)'
          }
        },
        data:data
        // data:[
        //   {name:"河北",value:10,itemStyle:{normal:{areaColor:"#f00"}}}
        // ]
      }]
    };
    myChart.setOption(option);
  }
}
export default tools
