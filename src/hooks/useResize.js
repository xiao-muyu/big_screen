import { debounce } from "@/utils";
import { ref, onMounted, onUnmounted } from 'vue';
  
 export const useResize = function(){
  // 声明一个当前适配的echarts实例
    let currentEchartEl = ref(null)
    let resizeHandler = ref(null)
    onMounted(() => {
        initListener()
    })
    onUnmounted(() => {
          destroyListener()
      })
    const initListener = () => {
        resizeHandler = debounce(() => {
            resize()
        }, 300)
        window.addEventListener('resize', resizeHandler)
    }
    const destroyListener = () => {
        //删除事件监听程序  
        window.removeEventListener('resize', resizeHandler)
        // 销毁echarts实例
        currentEchartEl.value.dispose();
        resizeHandler = null
    }
    const resize = () => {
        // console.log(111111122,currentEchartEl)
        currentEchartEl && currentEchartEl.value.resize()
    }

    return {
      currentEchartEl

      // 这个不用
      // initListener
    }

}