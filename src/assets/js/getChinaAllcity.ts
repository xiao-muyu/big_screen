// 获取中国所有城市的map集合  来源阿里云DataV http://datav.aliyun.com/portal/school/atlas/area_selector
import axios from 'axios'

// 引入type
import { Feature, Geodatav, citymodel } from "@/model/geodatav";


// 获取当前json下的省份城市区域adcode和json
// 根据城市adcode
function getCurrentadcode(mapdata: Geodatav) {
    let currentMap = new Map();
    
    mapdata.features.map((item:Feature)=>{
        if (item.properties.name != '') {
            let cityinfo: citymodel = {
                name: item.properties.name,
                adcode: item.properties.adcode,
                childrenNum: item.properties.childrenNum,
                url: `https://geo.datav.aliyun.com/areas_v3/bound/${item.properties.adcode}_full.json`,
                center: item.properties.center,
                parentadcode: item.properties.parent?.adcode,
                hasRegister: false
            }
            currentMap.set(cityinfo.adcode, cityinfo);
        }
    })
    return currentMap;
}

// 根据城市名称name
function getCurrentadcodebyname(mapdata: Geodatav) {
    let currentMap = new Map();
    
    mapdata.features.map((item:Feature)=>{
        if (item.properties.name != '') {
            let cityinfo: citymodel = {
                name: item.properties.name,
                adcode: item.properties.adcode,
                childrenNum: item.properties.childrenNum,
                url: `https://geo.datav.aliyun.com/areas_v3/bound/${item.properties.adcode}_full.json`,
                center: item.properties.center,
                parentadcode: item.properties.parent?.adcode,
                hasRegister: false
            }
            currentMap.set(cityinfo.name, cityinfo);
        }
    })
    return currentMap;
}



export { getCurrentadcode, getCurrentadcodebyname }

