import {get } from '../utils/http'

// 监控情况
export const getMonitoringSituation = () => {
    return get<any>('/safeManage/monitoringSituation')
  }