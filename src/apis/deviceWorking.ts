import {get } from '../utils/http'

// 设备统计数据
export const getEquipment = () => {
    return get<any>('/deviceWorking/equipment')
}
  
// 实时用电数据
export const getElectricityConsumption = () => {
    return get<any>('/deviceWorking/electricityConsumption')
}
  
// 场地使用数据
export const getVenueUse = () => {
    return get<any>('/deviceWorking/venueUse')
}
  
// 实时警报数据
export const getAllPoliceData = () => {
    return get<any>('/deviceWorking/allPoliceData')
}
  
// 设备检测数据
export const getEquipmentTesting = () => {
    return get<any>('/deviceWorking/equipmentTesting')
}
  