import {get } from '../utils/http'

type ResultItem = {
  salary: number;
  gradYear: number;
  total: number;
} 
interface StudentStats {
  result: ResultItem[]
  [keys: string]: any;
}

export interface StatsDistribution {
  result: {workCity:string,total: number;}[]
  [keys: string]: any;
}

type expertiseIdType = {
  expertiseId:number
}
// 获取毕业生薪资分布
export const getStudentStatsSalary = () => {
  return get<StudentStats>('/student/stats/salary')
}


// 统计学生毕业各个城市分布
// 使用get方法请求的时候规定这个请求返回的数据是什么类型的.
export const getStudentStatsDistribution = (data:expertiseIdType) => {
  return get<StatsDistribution>('/student/stats/distribution',data)
}

// 统计当年学生状态
export const getStudentStatsStatus = (data:expertiseIdType) => {
  return get<StatsDistribution>('/v1.0/api/student/stats/status',data)
}
//智能检测/水电用量测量
export const getStudentStatsDistrRightTop = () => {
  return get<any>('/intelligentDetection/dataTop')
}
//智能检测/校园人员流动情况
export const getStudentStatsDistributions = () => {
  return get<any>('/intelligentDetection/dataBottom')
}
//智能检测/学生分布情况
export const getStudentStatsDistriLeftBottom = () => {
  return get<any>('/intelligentDetection/data')
}
//智能检测/室内环境监测
export const getStudentStatsDistriLeftTop = () => {
  return get<any>('/intelligentDetection/dataLeftTop')
}

export const entranceGuardSituation= () => {
    return get('/safeManage/entranceGuardSituation')
  }
  export const monitoringSituation= () => {
    return get('/safeManage/monitoringSituation')
  }
  export const dutySituation= () => {
    return get('/safeManage/dutySituation')
  }