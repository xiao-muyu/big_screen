
import cityLat from "../assets/json/citylatlng.json"; //该文件路径改成自己项目中的文件路径即可

/**
 * 
 * @params code {string} 城市编码
 * @return [经度,维度]
 * @des:根据城市编码获取城市经纬度
 * @Date 2023年3月30日
 * @author suming
 * 
 */
export  function codeToLonLat(code){
  for (let item of cityLat){
    if(code == item.no.slice(0,4)){
      return item.latlng.split(",").reverse().map((item)=>{return item*1});
    }
  }
}