import axios, {
  InternalAxiosRequestConfig,
  AxiosRequestConfig,
  AxiosResponse,
  AxiosError,
  AxiosInstance
} from "axios";
// import qs from "qs";

type Response<T> = {
  code: number;
  message: string;
  // data: T |string; //这里是规定后端的接口返回的可以是传入的类型或者是string
  data: T ;
  [keys: string] : any;
}


const http:AxiosInstance  = axios.create({
  // baseURL:'http://47.93.101.203:3002/v1.0/api/'
  baseURL:'http://localhost:3002/v1.0/api/'
});

// 添加请求拦截器
http.interceptors.request.use(function (config: InternalAxiosRequestConfig) {
  config.params = {
    ...config.params,
    // token: "1ec949a15fb709370f",
  };
  if (config.data && !(config.data instanceof FormData)) {
    let token = localStorage.getItem("token");
    if (token) {
      config.data.token = token
    }
    // config.data = qs.stringify(config.data);
  }
  return config;
});

// 添加响应拦截器
http.interceptors.response.use(
  function (response: AxiosResponse) {
    if(response.data?.code === 302) {
      window.location.href = '/login';
    }
    return response;
  },
  function (error: AxiosError) {
    return Promise.reject(error);
  }
);

export const get = <T = any, P = any, R = Response<T>>(
  url: string,
  data?: P
): Promise<R> => {
  return new Promise((resolve, reject) => {
    http
      .get(url, { params: data })
      .then(({ data }) => resolve(data))
      .catch(resolve);
  });
};

// export const get= ()=>{}
export function post<T = any, P = any, R = Response<T>>(
  url: string,
  data?: P
): Promise<R> {
  return new Promise((resolve, reject) => {
    http
      .post(url, data)
      .then(({ data }) => resolve(data))
      .catch(resolve);
  });
}
